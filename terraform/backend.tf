terraform{
    backend "s3" {
        bucket = "micro-service-app-15-03-2023"
        region = "us-east-1"
        key ="app-eks-cluster/terraform.tfstate"
    }
}
//terraform {
  //backend "local" {
   // path = "terraform.tfstate"
  //}
//}
